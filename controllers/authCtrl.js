const Akun = require('../models').Akun
const Pelanggan = require('../models').Pelanggan
const Penjahit = require('../models').Penjahit

class AuthCtrl {
  static doLogin (req, res, next) {
    let email = req.body.email
    let password = req.body.password
    Akun.find({email: email, password: password}).then((data) => {
      console.log(data);
      if (data[0].tipe_akun == 'pelanggan') {
        console.log('sini');
        Pelanggan.find({id_akun: data[0]._id}).then((data1) => {
          res.send({
            message: 'Login Success',
            dataAkun: data[0],
            dataPelanggan: data1[0]
          })
        }).catch((err1) => {
          res.status(400).send({
            message: 'Login Gagal',
            err: err
          })
        })
      } else {
        console.log('sana');
        Penjahit.find({id_akun: data[0]._id}).then((data1) => {
          res.send({
            message: 'Login Success',
            dataAkun: data[0],
            dataPenjahit: data1[0]
          })
        }).catch((err1) => {
          res.status(400).send({
            message: 'Login Gagal',
            err: err
          })
        })
      }
    }).catch((err) => {
      res.status(400).send({
        message: 'Login Gagal',
        err: err
      })
    })
  }
  static signUp (req, res, next) {
    if (req.body.jenisUser == 'pelanggan') {
      Akun.create({
        tipe_akun: 'pelanggan',
        email: req.body.email,
        password: req.body.password
      }).then((data) => {
        Pelanggan.create({
          nama: req.body.nama,
          no_telp: req.body.no_telp,
          email: req.body.email,
          id_akun: data._id
        }).then((data1) => {
          res.send({
            message: 'Request Success',
            dataPelanggan: data1,
            dataAkun: data,
          })
        }).catch((err1) => {
          console.log(err);
          res.status(400).send({
            message: 'Request Failed',
            err
          })
        })
      }).catch((err) => {
        console.log(err);
        res.status(400).send({
          message: 'Request Failed',
          err
        })
      })
    } else {
      Akun.create({
        tipe_akun: 'penjahit',
        email: req.body.email,
        password: req.body.password
      }).then((data) => {
        Penjahit.create({
          nama: req.body.nama,
          no_telp: req.body.no_telp,
          email: req.body.email,
          nama_tailor: req.body.nama_tailor,
          alamat_tailor: req.body.alamat_tailor,
          id_akun: data._id
        }).then((data1) => {
          res.send({
            message: 'Sign Up Success',
            dataPenjahit: data1,
            dataAkun: data
          })
        }).catch((err1) => {
          console.log(err);
          res.status(400).send({
            message: 'Sign Up Gagal',
            err
          })
        })
      }).catch((err) => {
        console.log(err);
        res.status(400).send({
          message: 'Sign Up Gagal',
          err
        })
      })
    }
  }
}

module.exports = AuthCtrl;

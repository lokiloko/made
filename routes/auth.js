var express = require('express');
var router = express.Router();
const AuthCtrl = require('../controllers/authCtrl')

router.post('/login', AuthCtrl.doLogin)
router.post('/signup', AuthCtrl.signUp)

module.exports = router;

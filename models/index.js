const Akun = require('./akun');
const Atasan = require('./atasan');
const Bawahan = require('./bawahan');
const Pelanggan = require('./pelanggan');
const Penjahit = require('./penjahit');
const Pesanan = require('./pesanan');

module.exports = {
  Akun,
  Atasan,
  Bawahan,
  Pelanggan,
  Penjahit,
  Pesanan
}

const mongoose    = require('mongoose')
const Schema      = mongoose.Schema

const pesananSchema = new Schema({
  id_pelanggan: {
    type: String,
    required: true,
    ref: 'Pelanggan'
  },
  id_penjahit: {
    type: String,
    required: true,
    ref: 'Penjahit'
  },
  id_atasan: {
    type: String,
    required: true,
    ref: 'Atasan'
  },
  id_bawahan: {
    type: String,
    required: true,
    ref: 'Bawahan'
  },
  tanggalorder_pesanan: {
    type: Date
  },
  tanggalselesai_pesanan: {
    type: Date
  },
  status_pesanan: {
    type: String,
  }
}, {
  toObject: {
    virtuals: true
  },
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

const Model = mongoose.model('Pesanan', pesananSchema)
module.exports = Model

const mongoose    = require('mongoose')
const Schema      = mongoose.Schema

const pelangganSchema = new Schema({
  nama: {
    type: String,
    required: true
  },
  no_telp: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  id_akun: {
    type: String,
    required: true,
    ref: 'Akun'
  }
}, {
  toObject: {
    virtuals: true
  },
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

const Model = mongoose.model('Pelanggan', pelangganSchema)
module.exports = Model

const mongoose    = require('mongoose')
const Schema      = mongoose.Schema

const bawahanSchema = new Schema({
  jenis_bawahan: {
    type: String,
    required: true
  },
  gambardesain: {
    type: String,
    required: true
  },
  lebarpinggang: {
    type: Number,
    required: true
  },
  lebarpinggul: {
    type: Number,
    required: true
  },
  panjang: {
    type: Number,
    required: true
  },
  keterangan: {
    type: String,
    required: true
  },
}, {
  toObject: {
    virtuals: true
  },
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

const Model = mongoose.model('Bawahan', bawahanSchema)
module.exports = Model

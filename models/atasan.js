const mongoose    = require('mongoose')
const Schema      = mongoose.Schema

const atasanSchema = new Schema({
  jenis_atasan: {
    type: String,
    required: true
  },
  gambardesain: {
    type: String,
    required: true
  },
  lingkardada: {
    type: Number,
    required: true
  },
  panjangtangan: {
    type: Number,
    required: true
  },
  panjangbaju: {
    type: Number,
    required: true
  },
  lingkarlengan: {
    type: Number,
    required: true
  },
  lebarbahu: {
    type: Number,
    required: true
  },
  keterangan: {
    type: String,
    required: true
  },
}, {
  toObject: {
    virtuals: true
  },
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

const Model = mongoose.model('Atasan', atasanSchema)
module.exports = Model

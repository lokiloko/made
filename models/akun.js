const mongoose    = require('mongoose')
const Schema      = mongoose.Schema

const akunSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  tipe_akun: {
    type: String,
    required: true
  },
}, {
  toObject: {
    virtuals: true
  },
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

const Model = mongoose.model('Akun', akunSchema)
module.exports = Model

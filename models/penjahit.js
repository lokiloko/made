const mongoose    = require('mongoose')
const Schema      = mongoose.Schema

const penjahitSchema = new Schema({
  nama: {
    type: String,
    required: true
  },
  no_telp: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  nama_tailor: {
    type: String,
    required: true
  },
  alamat_tailor: {
    type: String,
    required: true
  },
  id_akun: {
    type: String,
    required: true,
    ref: 'Akun'
  },
}, {
  toObject: {
    virtuals: true
  },
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

const Model = mongoose.model('Penjahit', penjahitSchema)
module.exports = Model
